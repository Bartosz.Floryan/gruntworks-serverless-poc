# gruntworks-serverless-poc

It's lambda based project example to expose basic API. Project contains 2 lambda functions code. 
Both lambdas should be attached to API Gateway. It's example of backend service used by frontend application.

### infra schema
![diagram](./diagram.png)

```/functions/src``` contains 2 lambda functions code:
* ```get-user-details.ts```
* ```get-user-details.ts```


Functions look for ```{userId}``` path parameter in triggering event.
Lambda code uses common functions placed in ```\function\src\utils``` - this is example of common used validation or response template. 
Lambda triggered by API Gateway endpoint, returns JSON response with random/mock values
### API response example:

GET /user/11/name
```
{"id":"11","name":"Mike"}
```

GET /user/11/details
```
{"role":"admin","systemId":"usr_11","accessLevel":0.22557452913967557}
```

GET /user/wqe/name
```
Error for id: wqe
```