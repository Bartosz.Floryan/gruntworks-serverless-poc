export function errorResponse(body: string) {
    return {
        statusCode: 500,
        body: body
    };
}