import {Logger} from "@aws-lambda-powertools/logger";

const logger = new Logger()
export function isValidUserId (userId: string|undefined): boolean {
    logger.info(`Validation for id: ${userId}`)
  return userId !== undefined && parseInt(userId, 10) > 0;
}

