import {isValidUserId} from "./utils/user-id-validation";
import {errorResponse} from "./utils/api-response-template";
import {Logger} from "@aws-lambda-powertools/logger";
import {APIGatewayProxyHandler} from "aws-lambda";

const logger = new Logger({serviceName: 'findUser service'})

export const handler: APIGatewayProxyHandler = async (event) => {
  const userId = event.pathParameters!.userId;
  if(isValidUserId(userId)){
    logger.info(`Valid id`)
    return {
      statusCode: 200,
      body: JSON.stringify(findUserDetails(userId)),
    };
  } else {
    logger.error(`Invalid id`)
    return errorResponse(`No user details for id: ${userId}`)
  }
}

function findUserDetails(userId: any) {
  logger.info(`Search user details for id: ${userId}`)
  const role = ['user', 'admin', 'mod', 'read-user', 'locked-account']
  return {
    role: role[Math.floor(Math.random() * role.length)],
    systemId: `usr_${userId}`,
    accessLevel: Math.random()
  }
}
