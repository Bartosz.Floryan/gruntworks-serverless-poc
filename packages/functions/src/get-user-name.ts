import {isValidUserId} from "./utils/user-id-validation";
import {errorResponse} from "./utils/api-response-template";
import {Logger} from "@aws-lambda-powertools/logger";
import {APIGatewayProxyHandler} from "aws-lambda";

const logger = new Logger({serviceName: 'findUser service'})

export const handler: APIGatewayProxyHandler = async (event) => {
  const userId = event.pathParameters!.userId;
  if(isValidUserId(userId)){
    logger.info(`Valid id`)
    return {
      statusCode: 200,
      body: JSON.stringify({
        id: userId,
        userName: findUserName(userId)
      })
    };
  } else {
    logger.error(`Invalid id`)
    return errorResponse(`Error for id: ${userId}`)
  }
};

function findUserName(userId: any): string {
  logger.info(`Search user name for id: ${userId}`)
  const userNames = ['John', 'Julie', 'Tina', 'Tom', 'Mike']
  return userNames[Math.floor(Math.random() * userNames.length)]
}
